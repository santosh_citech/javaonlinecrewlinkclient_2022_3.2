import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CrewlinkdashboardComponent } from './crewlinkdashboard.component';

describe('CrewlinkdashboardComponent', () => {
  let component: CrewlinkdashboardComponent;
  let fixture: ComponentFixture<CrewlinkdashboardComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CrewlinkdashboardComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CrewlinkdashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
