import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
    providedIn: 'root',
})
export class dashboardService {

    constructor(private http: HttpClient) { }

    getRecords() {
       return this.http.get("../assets/json/data.json");
    }

}