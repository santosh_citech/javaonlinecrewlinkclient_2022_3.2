import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { CrewlinkdashboardComponent } from "./crewlinkdashboard.component";
const routes: Routes = [
   
    { path:"dashboard", component: CrewlinkdashboardComponent },
    
];

@NgModule({
    exports: [RouterModule],
    imports:[RouterModule.forChild(routes)]
})

export class crewLinkDashboardRoutingModule{}