import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { CrewlinkdashboardComponent } from "./crewlinkdashboard.component";
import { crewLinkDashboardRoutingModule } from "./routing";
import { FormsModule,ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';  

import {dashboardService}  from "./dashboard.service"
@NgModule({
    imports: [crewLinkDashboardRoutingModule,HttpClientModule,CommonModule,FormsModule,ReactiveFormsModule],
    declarations: [CrewlinkdashboardComponent],
    exports: [CrewlinkdashboardComponent],
    providers: [dashboardService]

})
export class CrewlinkDashboardModule { }