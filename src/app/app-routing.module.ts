import { NgModule } from '@angular/core';
import { CommonModule, } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { Routes, RouterModule } from '@angular/router';

import { AdminLayoutComponent } from './layouts/admin-layout/admin-layout.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { CrewlinkdashboardComponent } from './crewlinkdashboard/crewlinkdashboard.component';
import { LoginComponent } from "./login/login.component";
import { RegisterComponent } from "./register/register.component";
import { AuthGuard } from "./Auth.guard";

import { TrainsComponent } from './components/trains/trains.component';
import { UserComponent } from './components/user/user.component';
import { UserPlanComponent } from './components/user-plan/user-plan.component';
import { UploadsComponent } from './components/uploads/uploads.component';
import { TrainstationComponent } from './components/trainstation/trainstation.component';
import { DrivingsectionComponent } from './components/drivingsection/drivingsection.component';
import { StationComponent } from './components/station/station.component';
import { DivisionComponent } from './components/division/division.component';
import { CrewTypeComponent } from './components/crew-type/crew-type.component'

const routes: Routes = [

  { path: 'login', component: LoginComponent },
  { path: 'register', component: RegisterComponent },


  {
    path: '', component: AdminLayoutComponent,canActivate: [AuthGuard],
    children: [
      { path: 'dashboard', component: DashboardComponent },
    
      {
        path: "role",
        loadChildren: () => import("./role/role.module").then(x => x.RoleModule)
      },
      {
        path: "custom",
        loadChildren: () => import("./crewlinkdashboard/module").then(x => x.CrewlinkDashboardModule)
      },
      {
        path: "train",
        loadChildren: () => import("./components/trains/module").then(x => x.TrainModule)
      },
      {
        path: "trainstation",
        loadChildren: () => import("./components/trainstation/module").then(x => x.TrainstationModule)
      },
      {
        path: "drivingsection",
        loadChildren: () => import("./components/drivingsection/module").then(x => x.DrivingsectionModule)
      },
      {
        path: "division",
        loadChildren: () => import("./components/division/module").then(x => x.DivisionModule)
      },
      { path: 'user', component: UserComponent },
      { path: 'userplan', component: UserPlanComponent },
      { path: '**', redirectTo: '', pathMatch: 'full' }
    ],

  }

];

@NgModule({
  imports: [
    CommonModule,
    BrowserModule,
    RouterModule.forRoot(routes, {
      useHash: false
    })
  ],
  exports: [
  ],
  providers: [AuthGuard]
})
export class AppRoutingModule { }
