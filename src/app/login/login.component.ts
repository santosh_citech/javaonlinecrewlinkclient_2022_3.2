import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from "../authentication.service";
import { map } from 'rxjs/operators';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private authenticationService: AuthenticationService
  ) { }
  isLoggedIn = this.authenticationService.isLoggedIn();
  model: any = {};
  ngOnInit(): void {
  }

  login() {

    this.authenticationService.login(this.model.username, this.model.password).subscribe(
      response => {
        if (response.status == 200 && response.ok == true) {
          this.isLoggedIn = true;
        } else {

        }
      },

      err => console.error("failed authentication: " + err),
      () =>{ 
        window.sessionStorage["token"] = "125";
        window.sessionStorage["user"] = "santosh";
        this.router.navigate(['/dashboard']);

        console.log("tried authentication")
    },
    );


    // this.authenticationService.login(this.model.username, this.model.password).subscribe(
    //   (results: any) => {
    //     if (results) {
    //       // window.sessionStorage["token"] = result.token.token;
    //       // window.sessionStorage["user"] = result.token.userobj.username; 
    //       // window.sessionStorage["userRole"] = result.token.userobj.role;

    //       // localStorage.setItem("currentUser", JSON.stringify(result));
    //       console.log("login result"+results);
    //       this.router.navigate(['/dashboard']);
    //     }
    //     (error: any) => {
    //       console.log("login error result"+error);
    //     }
    //   });
  }

}
