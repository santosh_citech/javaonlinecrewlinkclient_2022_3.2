import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";



import { RoleComponent } from "./role.component";
import { CreateroleComponent } from './createrole/createrole.component';

const routes: Routes = [
    { path:"create", component: CreateroleComponent },
    { path: "list", component: RoleComponent },
];

@NgModule({
    exports: [RouterModule],
    imports: [RouterModule.forChild(routes)]
})

export class RoleRoutingModule { }