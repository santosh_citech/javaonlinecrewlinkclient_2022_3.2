import { Injectable } from '@angular/core';

import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { retry, catchError } from 'rxjs/operators';
import { Observable, throwError, map } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class RoleService {
  private apiUrl: string;

  httpOptions = {
    headers: new HttpHeaders(
      { 'Content-Type': 'application/json' },

    ).set('Authorization', 'Basic ' + btoa("admin" + ':' + "admin")
    )
  };
  constructor(private http: HttpClient) {
    this.apiUrl = 'http://localhost:6060'
  }

  createRole(roledetails: any = {}): Observable<any> {
    return this.http.post(this.apiUrl + '/api/v1/role/createrole', roledetails, this.httpOptions)

  }

  getAllRoles(itemsPerPage: any,currentPage:any,sort:any) {

    // window.location.href = '/login';
    let params = "?"+"sort="+sort+"&"+"page="+currentPage+"&"+"size="+itemsPerPage
    return this.http.get<any[]>(this.apiUrl + '/findByAllRoles'+params, this.httpOptions).pipe(map((data: any) => data),
      catchError(error => 'error')
    );
  }

  listAllRoles(itemsPerPage: any,currentPage:any,sort:any) {

    // window.location.href = '/login';
    let params = "?"+"sort="+sort+"&"+"page="+currentPage+"&"+"size="+itemsPerPage
    return this.http.get<any[]>(this.apiUrl + '/listAllRoles'+params, this.httpOptions).pipe(map((data: any) => data),
      catchError(error => 'error')
    );
  }



}
