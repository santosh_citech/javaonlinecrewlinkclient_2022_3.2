import { Component, OnInit } from '@angular/core';
import { RoleService } from "./role.service"

@Component({
  selector: 'app-role',
  templateUrl: './role.component.html',
  styleUrls: ['./role.component.css']
})
export class RoleComponent implements OnInit {


  itemsPerPage: any = 10;
  currentPage: any = 0;
  sort:any = "";
  public rolesList: any[] = [];

  public isLoading: boolean = false;
  perPage: any;
  totalPages: any;
  totalElements: any;



  constructor(
    private roleService: RoleService
  ) { }

  ngOnInit(): void {
    this.getroles(null);
  }

  getroles(event: any) {
    this.isLoading = true;
    this.roleService.listAllRoles(this.itemsPerPage,this.currentPage,this.sort)
      .subscribe((results: any) => {
          if (results) {
            this.rolesList = results.data;
            this.totalPages = results.totalPages;
            this.totalElements = results.totalElements;

            this.isLoading = false;
          }
          (error: any) => {
            console.log("error:"+error);
          }
        });

  }
  selectRole(r: any) {

  }
  removeRole(r: any) {

  }

}
