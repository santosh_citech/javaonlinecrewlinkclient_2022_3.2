import { Component, OnInit } from '@angular/core';
import { RoleService } from "../role.service";
import { Subscription } from 'rxjs';
import { timer } from 'rxjs';

export interface IUser {

  _id?: number
  username: string,
  firstname: string,
  lastname: string,
  password: string,
  email: string,
  markdelete: false
  createdtime: Date
  isUserchecked?: boolean;
}
@Component({
  selector: 'app-createrole',
  templateUrl: './createrole.component.html',
  styleUrls: ['./createrole.component.css']
})
export class CreateroleComponent implements OnInit {

  userdetails: any = {
    role: "",
    roleType: "",
  };
  query: any = {
    page: 1,
    size: 10
  }
  roles: any = [];
  usersList: IUser[] = [];
  itemsPerPage: any = 50;
  currentPage: any = 1;
  isSelected: boolean;
  query0: any = {
    page: this.currentPage,
    limit: this.itemsPerPage,
    order: "",
  }
  message: string = "";
  public showloader: boolean = false;      
  private subscription: Subscription;
  timercount:any; 

  constructor(
    private roleService: RoleService
  ) { }

  ngOnInit(): void {
  
   
  }

  public setTimer(){
    this.showloader = true;
    this.timercount = timer(5000); // 5000 millisecond means 5 seconds
    this.timercount.subscribe(() => {
        this.showloader = false;
    });
  }

 



  

}
