import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LSidebarComponent } from './lsidebar.component';

describe('LSidebarComponent', () => {
  let component: LSidebarComponent;
  let fixture: ComponentFixture<LSidebarComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LSidebarComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LSidebarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
