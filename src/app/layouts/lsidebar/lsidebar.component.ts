import { Component, OnInit, AfterViewInit } from '@angular/core';
declare var $: any;
@Component({
  selector: 'app-lsidebar',
  templateUrl: './lsidebar.component.html',
  styleUrls: ['./lsidebar.component.css']
})
export class LSidebarComponent implements OnInit, AfterViewInit {

  isLoggedInUser: any = "Santosh";
  isLoggedInUserRole: any = "Administrator";
  isLoggedIn = "online";

  constructor() { }

  ngOnInit(): void {
  }

  init_sidebar() {
    // $(".sidebar-dropdown > a").click(() => {
    //   $(".sidebar-submenu").slideUp(200);
    //   if (
    //     $(this).parent().hasClass("active")
    //   ) {
    //     $(".sidebar-dropdown").removeClass("active");
    //     $(this).parent().removeClass("active");
    //   } else {
    //     $(".sidebar-dropdown").removeClass("active");
    //     $(this)
    //       .next(".sidebar-submenu")
    //       .slideDown(200);
    //     $(this)
    //       .parent()
    //       .addClass("active");
    //   }
    // });


    var parent = $(this).parent().hasClass("active");


    $(".sidebar-dropdown > a").click(() => {
      // $(".sidebar-submenu").animate({ height: 'toggle', opacity: 'toggle' }, 'slow');
      console.log(parent)
      if ($(this).parent().hasClass("active")) {
        $(".sidebar-submenu").slideDown(200);
        $(".sidebar-dropdown").removeClass("active");
        $(this).parent().removeClass("active");

      } else {
        $(".sidebar-dropdown").removeClass("active");
        // $(this).next(".sidebar-submenu").slideUp(200);
        $(".sidebar-submenu").slideUp(200);
        $(this).parent().addClass("active");

      }
      console.log("kkk");

    });

  }


  showHideSideBar() {
    $("#close-sidebar").click(() => {
      $(".page-wrapper").removeClass("toggled");
    });
    $("#show-sidebar").click(() => {
      $(".page-wrapper").addClass("toggled");
    });
  }

  ngAfterViewInit(): void {

    this.init_sidebar();
    // this.InitializeQuery();
    this.showHideSideBar();

  }


}
