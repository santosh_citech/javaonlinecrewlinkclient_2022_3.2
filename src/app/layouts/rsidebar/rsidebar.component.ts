import { Component, OnInit, AfterViewInit } from '@angular/core';
declare var $: any;
@Component({
  selector: 'app-rsidebar',
  templateUrl: './rsidebar.component.html',
  styleUrls: ['./rsidebar.component.css']
})
export class RsidebarComponent implements OnInit, AfterViewInit {

  constructor() { }

  ngOnInit(): void {
  }

  showHideSideBar() {

    var xthis: any = $(this);

    // console.log(xthis);
    var parent = $(this).parent().hasClass("right-toggled");
    var parent0 = $(this).parent().hasClass("sideBarRight");
    if (!parent) {
      $(".sideBarRight").addClass("right-toggled");
      // $("#hide-rightsidebar").css('visibility','hidden');

    }
    $("#hide-rightsidebar").click(() => {
      $(".sideBarRight").addClass("right-toggled");
      // $("#hide-rightsidebar").hide();
      $("#hide-rightsidebar").css('visibility','visible');
      // $("#show-rightsidebar").show();
    });
    $("#show-rightsidebar").click(() => {
      // $(".page-wrapper").addClass("toggled");
      $(".sideBarRight").removeClass("right-toggled");
      // $("#show-rightsidebar").show();
    });
  }

  ngAfterViewInit(): void {

    this.showHideSideBar();

  }

}
