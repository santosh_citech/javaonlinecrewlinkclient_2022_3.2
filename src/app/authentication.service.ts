import { Injectable } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { retry, catchError } from 'rxjs/operators';
import { Observable, throwError } from 'rxjs';

import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {
  isLogged: boolean = false;
  userobj: any = { user: "Raj", userRole: "admin" };
  apiUrl = 'http://localhost:6060';
 
 

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private http: HttpClient
  ) { }

  isLoggedIn() {
    return this.check();
  }
  check() {
    if (window.sessionStorage["token"] && window.sessionStorage["user"]) {
      this.isLogged = true;
    } else {
      this.isLogged = false;
      delete this.userobj.user;
    }
    return (this.isLogged) ? this.isLogged : false;
  }

  logout() {
    if (this.isLogged) {
      this.isLogged = false;
      delete this.userobj.user;
      delete this.userobj.userRole;
      delete window.sessionStorage["userPlan"];
      delete window.sessionStorage["token"];
      delete window.sessionStorage["user"];
      delete window.sessionStorage["userRole"];
      localStorage.removeItem('currentUser');
      window.sessionStorage.clear();
      this.router.navigate(['/login']);

    }
  }
  login(username: string, password: string): Observable<any> {
   
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        "Authorization": "Basic " + btoa(username + ":" + password)
      }
      ).set('Access-Control-Allow-Origin', '*')
    };

    return this.http.post<any[]>(this.apiUrl+"/login",{"username":username,"password":password},httpOptions).pipe(map((data: any) => data),
      catchError(error => 'error')
    );

  }
}
