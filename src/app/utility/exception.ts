export default class DataError {
    constructor(message: string) {
        const error = Error(message);

        // set immutable object properties
        Object.defineProperty(error, 'message', {
            get() {
                return message;
            }
        });
        Object.defineProperty(error, 'name', {
            get() {
                return 'DataError';
            }
        });
        // capture where error occured
        Error.captureStackTrace(error, DataError);
        return error;
    }
}