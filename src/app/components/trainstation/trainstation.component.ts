import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { TrainTimeTableService } from "./traintimetable.service";

@Component({
  selector: 'app-trainstation',
  templateUrl: './trainstation.component.html',
  styleUrls: ['./trainstation.component.css']
})
export class TrainstationComponent implements OnInit {
  public isLoading: boolean = false;
  public itemsPerPage: number = 100;
  public currentPage: number = 1;
  public trainStationsResult: any[] = [];
  public query: any = {
    page: this.currentPage,
    limit: this.itemsPerPage,
    order: "",
    trainno: "",
    startDays: []
  }
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private trainTimeTableService: TrainTimeTableService
  ) { }

  ngOnInit(): void {
    this.route
    .queryParams
    .subscribe(params => {
      this.query.trainno = params['trainNo'];
      this.query.startDays = params['startDay'];
      console.log(params['trainNo'])
      console.log(params['startDay'])

    });
  }

}
