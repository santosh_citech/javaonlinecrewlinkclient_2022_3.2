import { NgModule } from '@angular/core';
import { TrainstationComponent } from "./trainstation.component";
import { TrainstationRoutingModule } from "./routing";
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { TrainTimeTableService} from './traintimetable.service';
@NgModule({
    imports: [TrainstationRoutingModule, CommonModule, FormsModule, ReactiveFormsModule],
    declarations: [TrainstationComponent],
    exports: [TrainstationComponent],
    providers: [TrainTimeTableService]

})
export class TrainstationModule { }