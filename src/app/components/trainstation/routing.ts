import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { TrainstationComponent } from "./trainstation.component";

const routes: Routes = [

    { path: "list", component: TrainstationComponent },

];

@NgModule({
    exports: [RouterModule],
    imports: [RouterModule.forChild(routes)]
})

export class TrainstationRoutingModule { }