import { NgModule } from '@angular/core';
import { StationComponent } from "./station.component";
import { StationRoutingModule } from "./routing";
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
@NgModule({
    imports: [StationRoutingModule, CommonModule, FormsModule, ReactiveFormsModule],
    declarations: [StationComponent],
    exports: [StationComponent],
    providers: []

})
export class StationModule { }