import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { StationComponent } from "./station.component";
const routes: Routes = [

    { path: "list", component: StationComponent },

];

@NgModule({
    exports: [RouterModule],
    imports: [RouterModule.forChild(routes)]
})

export class StationRoutingModule { }