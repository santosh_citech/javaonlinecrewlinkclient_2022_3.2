import { NgModule } from '@angular/core';
import { UserComponent } from "./user.component";
import { UserRoutingModule } from "./routing";
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
@NgModule({
    imports: [UserRoutingModule, CommonModule, FormsModule, ReactiveFormsModule],
    declarations: [UserComponent],
    exports: [UserComponent],
    providers: []

})
export class UserModule { }