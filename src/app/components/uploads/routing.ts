import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { UploadsComponent } from "./uploads.component";

const routes: Routes = [

    { path: "list", component: UploadsComponent },

];

@NgModule({
    exports: [RouterModule],
    imports: [RouterModule.forChild(routes)]
})

export class UploadsRoutingModule { }