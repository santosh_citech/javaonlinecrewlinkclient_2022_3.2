import { NgModule } from '@angular/core';
import { UploadsComponent } from "./uploads.component";
import { UploadsRoutingModule } from "./routing";
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
@NgModule({
    imports: [UploadsRoutingModule, CommonModule, FormsModule, ReactiveFormsModule],
    declarations: [UploadsComponent],
    exports: [UploadsComponent],
    providers: []

})
export class UploadsModule { }