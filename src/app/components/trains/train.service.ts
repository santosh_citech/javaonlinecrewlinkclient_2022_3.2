import { Injectable } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { retry, catchError } from 'rxjs/operators';
import { Observable, throwError } from 'rxjs';

import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class TrainService {

  apiUrl = 'http://localhost:6060';


 
  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private http: HttpClient
  ) {



  }


  getTrains(query: any) {
    let httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        "Authorization": "Basic " + btoa("admin123" + ":" + "admin123")
      })
    };
  
    let Url = this.apiUrl + "/listAllTrains"+ "?" + "sort=" + query.sort + "&" + "page=" + query.page + "&" + "size=" + query.size
    return this.http.get<any[]>(Url, httpOptions).pipe(map((data: any) => data),
      catchError(error => 'error')
    );
  }


  saveUser(model: any = {}): void {

    // return this.http.post(this.apiUrl + "/api/v1/user/saveUser", model, this.httpOptions)
    // return null;
        
  }
}
