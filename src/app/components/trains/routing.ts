import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { TrainsComponent } from "./trains.component";
import { CreateComponent } from "./create/create.component";
const routes: Routes = [

    { path: "list", component: TrainsComponent },
    { path: "create", component: CreateComponent },

];

@NgModule({
    exports: [RouterModule],
    imports: [RouterModule.forChild(routes)]
})

export class TrainRoutingModule { }