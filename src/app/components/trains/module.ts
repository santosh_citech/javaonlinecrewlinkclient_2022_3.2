import { NgModule } from '@angular/core';
import { TrainsComponent } from "./trains.component";
import { CreateComponent } from "./create/create.component";
import { TrainRoutingModule } from "./routing";
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import {TrainService} from "./train.service"
@NgModule({
    imports: [TrainRoutingModule, CommonModule, FormsModule, ReactiveFormsModule],
    declarations: [TrainsComponent,CreateComponent],
    exports: [TrainsComponent,CreateComponent],
    providers: [TrainService]

})
export class TrainModule { }