import { Component, OnInit } from '@angular/core';
import {TrainService} from "./train.service"
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
@Component({
  selector: 'app-trains',
  templateUrl: './trains.component.html',
  styleUrls: ['./trains.component.css']
})
export class TrainsComponent implements OnInit {

  
  constructor(private trainService:TrainService,   private _router: Router) { }
  public trainLists: any[] = [];
  
  public days = ['sunday', 'monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday'];

  searchTerm: string = '';
  isLoading: boolean = false;
  public itemsPerPage: number = 10;
  public currentPage: number = 0;
  postsPerPage: number[] = [5, 10, 25, 50, 100];
  maxPages: number = 5;

  query: any = {
    page: this.currentPage,
    size: this.itemsPerPage,
    sort: "trainName",
    trainNumber: "",
    trainname: "",
    fromstation: "",
    tostation: ""
  }

  selectedCssClass:string = 'selected-train-section';
  perPage: any;
  totalPages: any;
  totalRecords: any;
  selectedTrain:any ={
    trainNo:12594,
    startDay:0

  }
  

  ngOnInit(): void {
    this.loadTrainsList();
  }

  loadTrainsList() {
    this.isLoading = true;
    this.trainService.getTrains(this.query).subscribe(
      (response:any) => {
        this.trainLists = response.data;
        this.isLoading = false;
    })
  }
  trainTimeTable(train: any = {}) {

    this._router.navigate(['trainstation/list'], { skipLocationChange: true, queryParams: { "trainNo": train.trainNo, "startDay": train.startDay, } });

  }

  public currentCompany: any;
  getSelectedTrainCss =  (trainItem: any) => {
    if (this.selectedTrain.trainNo == trainItem.trainNo
      &&this.selectedTrain.startDay == trainItem.startDay) {
    return this.selectedCssClass;
  }
  return "";
   
   
  };

}
