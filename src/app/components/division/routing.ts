import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { DivisionComponent } from "./division.component";
const routes: Routes = [

    { path: "list", component: DivisionComponent },

];

@NgModule({
    exports: [RouterModule],
    imports: [RouterModule.forChild(routes)]
})

export class DivisionRoutingModule { }