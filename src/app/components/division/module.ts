import { NgModule } from '@angular/core';
import { DivisionComponent } from "./division.component";
import { DivisionRoutingModule } from "./routing";
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
@NgModule({
    imports: [DivisionRoutingModule, CommonModule, FormsModule, ReactiveFormsModule],
    declarations: [DivisionComponent],
    exports: [DivisionComponent],
    providers: []

})
export class DivisionModule { }