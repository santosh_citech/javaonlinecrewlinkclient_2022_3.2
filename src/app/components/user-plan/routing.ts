import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { UserPlanComponent } from "./user-plan.component";
const routes: Routes = [

    { path: "list", component: UserPlanComponent },

];

@NgModule({
    exports: [RouterModule],
    imports: [RouterModule.forChild(routes)]
})

export class UserPlanRoutingModule { }