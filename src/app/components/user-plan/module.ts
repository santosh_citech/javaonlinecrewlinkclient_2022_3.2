import { NgModule } from '@angular/core';
import { UserPlanComponent } from "./user-plan.component";
import { UserPlanRoutingModule } from "./routing";
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
@NgModule({
    imports: [UserPlanRoutingModule, CommonModule, FormsModule, ReactiveFormsModule],
    declarations: [UserPlanComponent],
    exports: [UserPlanComponent],
    providers: []

})
export class UserPlanModule { }