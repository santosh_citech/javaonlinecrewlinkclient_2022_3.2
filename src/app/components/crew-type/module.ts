import { NgModule } from '@angular/core';
import { CrewTypeComponent } from "./crew-type.component";
import { CrewTypeRoutingModule } from "./routing";
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
@NgModule({
    imports: [CrewTypeRoutingModule, CommonModule, FormsModule, ReactiveFormsModule],
    declarations: [CrewTypeComponent],
    exports: [CrewTypeComponent],
    providers: []

})
export class CrewTypeModule { }