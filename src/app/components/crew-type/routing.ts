import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { CrewTypeComponent } from "./crew-type.component";
const routes: Routes = [

    { path: "list", component: CrewTypeComponent },

];

@NgModule({
    exports: [RouterModule],
    imports: [RouterModule.forChild(routes)]
})

export class CrewTypeRoutingModule { }