import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CrewTypeComponent } from './crew-type.component';

describe('CrewTypeComponent', () => {
  let component: CrewTypeComponent;
  let fixture: ComponentFixture<CrewTypeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CrewTypeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CrewTypeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
