import { NgModule } from '@angular/core';
import { TrainsComponent } from "./trains.component";
import { TrainRoutingModule } from "./routing";
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
@NgModule({
    imports: [TrainRoutingModule, CommonModule, FormsModule, ReactiveFormsModule],
    declarations: [TrainsComponent],
    exports: [TrainsComponent],
    providers: []

})
export class TrainModule { }