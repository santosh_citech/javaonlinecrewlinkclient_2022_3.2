import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { TrainsComponent } from "./trains.component";
const routes: Routes = [

    { path: "list", component: TrainsComponent },

];

@NgModule({
    exports: [RouterModule],
    imports: [RouterModule.forChild(routes)]
})

export class TrainRoutingModule { }