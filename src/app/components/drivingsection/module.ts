import { NgModule } from '@angular/core';
import { DrivingsectionComponent } from "./drivingsection.component";
import { DrivingsectionRoutingModule } from "./routing";
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
@NgModule({
    imports: [DrivingsectionRoutingModule, CommonModule, FormsModule, ReactiveFormsModule],
    declarations: [DrivingsectionComponent],
    exports: [DrivingsectionComponent],
    providers: []

})
export class DrivingsectionModule { }