import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { DrivingsectionComponent } from "./drivingsection.component";
const routes: Routes = [

    { path: "list", component: DrivingsectionComponent },

];

@NgModule({
    exports: [RouterModule],
    imports: [RouterModule.forChild(routes)]
})

export class DrivingsectionRoutingModule { }