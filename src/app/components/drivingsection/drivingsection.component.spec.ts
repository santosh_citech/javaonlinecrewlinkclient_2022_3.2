import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DrivingsectionComponent } from './drivingsection.component';

describe('DrivingsectionComponent', () => {
  let component: DrivingsectionComponent;
  let fixture: ComponentFixture<DrivingsectionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DrivingsectionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DrivingsectionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
