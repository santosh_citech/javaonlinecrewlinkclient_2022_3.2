import { Component } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
@Component({
  selector: 'app-root',
  template: `<router-outlet></router-outlet>`,
})
export class AppComponent {
  title: string = "gonlineshopping"
  constructor(private router: Router,
    private route: ActivatedRoute
  ) {

    // this.router.navigate(['./home']);

  }

}
