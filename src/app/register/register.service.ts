import { Injectable } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { retry, catchError } from 'rxjs/operators';
import { Observable, throwError } from 'rxjs';

import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class RegisterService {

  apiUrl = 'http://localhost:6060';


  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' }).set('Access-Control-Allow-Origin', '*')
  };
  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private http: HttpClient
  ) { }

  loadServerData(): Observable<any> {
    return this.http.get(this.apiUrl, { responseType: 'text' })

  }

  getUsers(query: any) {
    let Url = this.apiUrl + "/api/v1/user/getALLUsers?" + "sort=" + query.sort + "&" + "page=" + query.page + "&" + "size=" + query.size
    return this.http.get<any[]>(Url).pipe(map((data: any) => data),
      catchError(error => 'error')
    );
  }


  saveUser(model: any = {}): Observable<any> {

    return this.http.post(this.apiUrl + "/api/v1/user/saveUser", model, this.httpOptions)

  }
}
