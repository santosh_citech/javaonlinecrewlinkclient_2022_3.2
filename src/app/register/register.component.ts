import { Component, OnInit } from '@angular/core';
import { RegisterService } from "./register.service";
import { NgForm } from '@angular/forms';

export class User{
  constructor(
    username: string,
    password: string,
    firstName: string,
    lastName: string,
    email:string,
    mobileNo:string,
    rolecode:string
  ){}
}

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  model = {
    username: "",
    password: "123458",
    firstName: "santosh",
    lastName: "sahu",
    email: "",
    mobileNo: "75412356",
    rolecode:"admin"
  }
  
  public roleCodesList: string[] = ["admin", "subscriber", "editor", "author", "contributor"];
  public userList: any[] = [];
  query = {
    size:10,
    page:1,
    sort:"asc"
  }
  isLoading:boolean = false;
  itemsPerPage:number = 10;
  currentPage:number = 1
  constructor(
    private registerService: RegisterService
  ) { }

  ngOnInit(): void {
    this.getUser()
    
  }

  getUser() {
    this.registerService.getUsers(this.query).subscribe(
      (response:any) => {
        this.userList = response.content;
      console.log( this.userList);
    })
  }

  saveUser(RegForm: NgForm) {
    this.registerService.saveUser(this.model).subscribe((result) => {
      console.log(result);
    }, error => {
      console.log("Error in saving" + error);
    })
  }

  addUser(RegForm: NgForm) { }

  removeUser(user: any = {}) {

  }
  selectUser(user: any = {}) {

  }

  updateUser(user: any) {
    if (user !== null && user !== undefined) {
      Object.assign(this.model, user);
    }
  }

  update() {


  }

}


