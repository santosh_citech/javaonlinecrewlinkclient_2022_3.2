<https://bootsnipp.com/snippets/Q0dAX>

# How add C3 charts to angular 2+ project

## npm install c3 && npm install @types/c3 --save-dev

1- run this command in your project root:

npm install c3
npm install @types/c3
2- add "../node_modules/c3/c3.min.css", to .angular-cli.json => style section

    (In angular 6+ add @import "../node_modules/c3/c3.min.css"; to styles.css)

3- add <div id="chart"></div> to your component template

4- add import * as c3 from 'c3'; to your component

5- add bellow codes to your typescript

ngAfterViewInit() {
    let chart = c3.generate({
    bindto: '#chart',
        data: {
            columns: [
                ['data1', 30, 200, 100, 400, 150, 250],
                ['data2', 50, 20, 10, 40, 15, 25]
            ]
        }
    });
}

# How add D3 charts to angular 2+ project

## npm install d3 && npm install @types/d3 --save-dev

Option 1 (Use of Standard D3 bundle)

npm install --save d3 (The normal approach to installing the Standard D3 v4 Bundle)
npm install --save-dev @types/d3 (Installs the definitions for the Standard D3 v4 Bundle) You should use --save, if you are building a library to be used by other Angular 2 applications, so that the library consumer can develop with typing support)
Now you can use import * as d3 from 'd3', e.g. in the module which creates a singleton D3 service to be injected into components on an as needed basis.
Option 2 (Use only what you need)

npm install --save d3-selection d3-transition d3-shape d3-scale Install only the modules you need for your project
npm install --save-dev @types/d3-selection @types/d3-transition @types/d3-shape @types/d3-scale (Install the matching definitions. Again, use --save instead of --save-dev depending on your use case.)
For convenience, you can now bundle these modules in a barrel module d3-bundle.ts
e.g.

// d3-bundle.ts
export *from 'd3-selection';
export* from 'd3-transition';
export *from 'd3-shape';
export* from 'd3-scale';
Import from the bundle (using the appropriate relative path) to create a singleton D3 service i.e. import * as d3 from './d3-bundle'
The approach under Option 2 is essentially what d3-ng2-service does. It can also give you an idea on how to build your own D3 Service, if the published one is not right for. You can always suggest improvements to it, of course.

I will say that, using the import-based options with angular-cli has become a lost easier since it changed to Webpack 2 for module-loading/bundling.

 // "node_modules/d3/dist/d3.min.js",
 // "node_modules/c3/c3.min.js"
